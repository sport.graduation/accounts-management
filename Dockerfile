FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 9600

COPY target/*.jar accountsManagement.jar

ENTRYPOINT [ "java","-jar","./accountsManagement.jar"]

package com.powerfit.accountsManagement.userDetailsRepo;

import java.util.Optional;

import com.powerfit.accountsManagement.model.Team;
import com.powerfit.accountsManagement.model.UserCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepo extends JpaRepository<Team,Long> {

    Optional<UserCredentials> findByName(String email);

}

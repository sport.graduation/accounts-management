package com.powerfit.accountsManagement.userDetailsRepo;

import com.powerfit.accountsManagement.model.Coach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoachRepo extends JpaRepository<Coach,Long> {
}

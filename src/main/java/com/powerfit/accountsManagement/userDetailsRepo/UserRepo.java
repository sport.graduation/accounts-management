package com.powerfit.accountsManagement.userDetailsRepo;


import java.util.Optional;

import javax.transaction.Transactional;

import com.powerfit.accountsManagement.model.UserCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserCredentials, Long> {
    Optional<UserCredentials> findByEmail(String email);

    boolean  existsByEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE UserCredentials u SET u.password = ?2 where u.id = ?1")
    void updatePassword(long id, String password);
}

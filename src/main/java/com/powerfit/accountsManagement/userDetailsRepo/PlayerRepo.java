package com.powerfit.accountsManagement.userDetailsRepo;

import javax.transaction.Transactional;

import com.powerfit.accountsManagement.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepo extends JpaRepository<Player,Long> {

    @Transactional
    @Modifying
    @Query("UPDATE Player u SET u.firebaseToken = ?2 where u.id = ?1")
    void updateFirebaseToken(long id, String token);
}

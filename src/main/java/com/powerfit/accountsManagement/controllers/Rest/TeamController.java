package com.powerfit.accountsManagement.controllers.Rest;

import java.util.List;

import com.powerfit.accountsManagement.services.TeamService;
import com.powerfit.accountsManagement.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/accounts/team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    Team getTeam(@PathVariable Long id) {
        return teamService.getTeam(id);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/update/{id}")
    ResponseEntity<?> updateTeam(@RequestBody Team updatedTeam,@PathVariable Long id) {
        return teamService.updatedTeam(updatedTeam, id);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/delete/{id}")
    ResponseEntity<?> deleteTeam(@PathVariable Long id) {
        return teamService.deleteTeam(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/all_team")
    List<Team> getAllTeam() {
        return teamService.getTeams();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getInfo/{id}")
    ResponseEntity<?> getTeamInfo(@PathVariable long id) {
        return teamService.getTeamInfo(id);
    }
}

package com.powerfit.accountsManagement.controllers.Rest;

import java.util.List;

import com.powerfit.accountsManagement.model.Player;
import com.powerfit.accountsManagement.services.PlayerService;
import com.powerfit.accountsManagement.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/accounts/player")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    ResponseEntity<?> getUser(@PathVariable Long id) {
        return playerService.getPlayer(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getTeamId/{id}")
    ResponseEntity<?> getTeamID(@PathVariable Long id) {
        return playerService.getPlayerTeam(id);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/update")
    ResponseEntity<?> updateUser(@RequestBody Player updatedPlayer) {
        return playerService.updatedPlayer(updatedPlayer);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/delete/{id}")
    ResponseEntity<?> deleteUser(@PathVariable Long id) {
        return playerService.deletePlayer(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/all_Player")
    ResponseEntity<?> getUser() {
        return playerService.getPlayers();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getPlayerByToken")
    public ResponseEntity<?> getByToken(@RequestHeader("Authorization") String token) {
        log.info("Trying to get user by token {}", token);
        return ResponseEntity.ok(playerService.getPlayer(userService.getUserByToken(token).getUser_id()));
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/sendMyToken/{id}")
    ResponseEntity<?> saveMyFirebaseToken(@RequestParam String token,@PathVariable long id){
        return playerService.saveMyFirebaseToken(token,id);
    }

}

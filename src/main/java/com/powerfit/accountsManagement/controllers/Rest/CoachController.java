package com.powerfit.accountsManagement.controllers.Rest;

import java.util.List;

import com.powerfit.accountsManagement.model.Coach;
import com.powerfit.accountsManagement.services.CoachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/accounts/coach")
public class CoachController {
    
    @Autowired
    private CoachService coachService;

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    Coach getUser(@PathVariable Long id) {
        return coachService.getCoach(id);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/update")
    ResponseEntity<?> updateUser(@RequestBody Coach updatedCoach) {
        return coachService.updatedCoach(updatedCoach);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/delete/{id}")
    ResponseEntity<?> deleteUser(@PathVariable Long id) {
        return coachService.deleteCoach(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/all_coach")
    List<Coach> getUser() {
        return coachService.getCoachs();
    }

}

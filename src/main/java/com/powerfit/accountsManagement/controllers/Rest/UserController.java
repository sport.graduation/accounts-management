package com.powerfit.accountsManagement.controllers.Rest;

import java.io.IOException;

import com.powerfit.accountsManagement.security.ConnValidationResponse;
import com.powerfit.accountsManagement.services.TokenVerifyService;
import com.powerfit.accountsManagement.services.UpdatePasswordService;
import com.powerfit.accountsManagement.request.CoachRegistrationRequest;
import com.powerfit.accountsManagement.request.PlayerRegistrationRequest;
import com.powerfit.accountsManagement.registration.RegistrationService;
import com.powerfit.accountsManagement.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Slf4j
@RequestMapping("/api/v1/accounts")
public class UserController {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private UpdatePasswordService updatePasswordService;

    @Autowired
    private TokenVerifyService tokenVerifyService;

    @Autowired
    private UserService userService;

    @RequestMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    String Login() {
        return "Welcome for Admin";
    }

    @RequestMapping("/player")
    @PreAuthorize("hasRole('ROLE_PLAYER')")
    String Hello() {
        return "Welcome for player";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/coach_signup")
    public void CoachSignUp(@RequestBody CoachRegistrationRequest coachRegistrationRequest) {
        registrationService.coachSignUp(coachRegistrationRequest);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/player_signup")
    public void PlayerSignUp(@RequestBody PlayerRegistrationRequest playerRegistrationRequest/*, @RequestParam("Photo") MultipartFile file*/) {
        registrationService.playerSignup(playerRegistrationRequest/*,file*/);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/verify")
    public String verify(@RequestParam("token") String token) {
        return registrationService.verifiedToken(token);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/player_edit_password/{id}")
    public void PlayerSignUp(@RequestBody String newPassword, @PathVariable Long id) {
        updatePasswordService.playerEditPassword(newPassword, id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/validateToken")
    public ResponseEntity<ConnValidationResponse> validateToken(@RequestHeader("Authorization") String token) {
        log.info("Trying to validate token {}", token);
        return ResponseEntity.ok(tokenVerifyService.validateToken(token));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getPhoto/{id}")
    public MultipartFile validateToken(@PathVariable Long id) throws IOException {
        log.info("Trying to validate token {}", id);
        return userService.getPhotosImagePath(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/photo")
    public void PlayerSignUp(@RequestParam("Photo") MultipartFile file) {
        registrationService.photo(file);
    }
}

package com.powerfit.accountsManagement.authentication;

import java.util.Optional;

import com.powerfit.accountsManagement.model.UserCredentials;
import com.powerfit.accountsManagement.userDetailsRepo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<UserCredentials> userCredentials = userRepo.findByEmail(email);

        return userCredentials
            .orElseThrow(() -> new UsernameNotFoundException("User Not found: " + email));
    }
}


package com.powerfit.accountsManagement.registration;

import static org.springframework.beans.MethodInvocationException.ERROR_CODE;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import com.powerfit.accountsManagement.model.Coach;
import com.powerfit.accountsManagement.model.Player;
import com.powerfit.accountsManagement.model.Team;
import com.powerfit.accountsManagement.request.CoachRegistrationRequest;
import com.powerfit.accountsManagement.request.PlayerRegistrationRequest;
import com.powerfit.accountsManagement.userDetailsRepo.PlayerRepo;
import com.powerfit.accountsManagement.userDetailsRepo.TeamRepo;
import com.powerfit.accountsManagement.userDetailsRepo.UserRepo;
import com.powerfit.accountsManagement.model.UserCredentials;
import com.powerfit.accountsManagement.registration.token.ConfirmationToken;
import com.powerfit.accountsManagement.registration.token.ConfirmationTokenService;
import com.powerfit.accountsManagement.userDetailsRepo.CoachRepo;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
public class RegistrationService {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private CoachRepo coachRepo;
    @Autowired
    private PlayerRepo playerRepo;
    @Autowired
    private TeamRepo teamRepo;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private ConfirmationTokenService confirmationTokenService;
    @Autowired
    private JavaMailSender mailSender;

    @Value("${application.mail.myEmail}")
    private String companyEmail;
    @Value("${application.mail.myCompanyName}")
    private String myCompanyName;
    @Value("${application.mail.siteURL}")
    private String siteURL;

    @Transactional
    public void coachSignUp(CoachRegistrationRequest coachRegistrationRequest) {
        boolean userExist = userRepo.findByEmail(coachRegistrationRequest.getEmail()).isPresent();
        if (userExist) {
            throw new IllegalStateException("Email: " + coachRegistrationRequest.getEmail() + " already taken");
        }

        coachRegistrationRequest.setPassword(passwordEncoder.encode(coachRegistrationRequest.getPassword()));

        UserCredentials newUser = new UserCredentials(
            coachRegistrationRequest.getPassword(),
            coachRegistrationRequest.getEmail(),
            "ROLE_ADMIN"
        );

        userRepo.save(newUser);

        Coach newCoach = new Coach(newUser.getUser_id(),
                                   null, coachRegistrationRequest.getPhoneNum(),
                                   coachRegistrationRequest.getFirstName(), coachRegistrationRequest.getLastName(),
                                   coachRegistrationRequest.getTeam());
        coachRepo.save(newCoach);

        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = new ConfirmationToken(
            token,
            LocalDateTime.now(),
            LocalDateTime.now().plusDays(1), newUser
        );

        confirmationTokenService.saveConfirmationToken(confirmationToken);

        try {
            sendVerificationEmail(newUser.getEmail(), newCoach.getFirstName(), newCoach.getLastName(), token);

        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void playerSignup(PlayerRegistrationRequest playerRegistrationRequest/*,
                             MultipartFile photoPlayerFile*/) {

        Optional<UserCredentials> userCredentials = userRepo.findByEmail(playerRegistrationRequest.getEmail());

        if (userCredentials.isPresent()) {
            throw new IllegalStateException("Email: " + playerRegistrationRequest.getEmail() + " already taken");
        } else {
            Optional<Team> playerTeam = teamRepo.findById(playerRegistrationRequest.getTeamID());
            if (playerTeam.isPresent()) {
                Team myTeam = playerTeam.get();

                System.out.println(myTeam.getName() + "***********");


                String password = generatePassword();
                String encodedPassword = (passwordEncoder.encode(password));

                UserCredentials newUser = new UserCredentials(
                    encodedPassword,
                    playerRegistrationRequest.getEmail(),
                    "ROLE_PLAYER"
                );

                userRepo.save(newUser);

                //   String photoPlayer = StringUtils.cleanPath(Objects.requireNonNull(photoPlayerFile.getOriginalFilename()));

                Player newPlayer = new Player(newUser.getUser_id(),
                                              playerRegistrationRequest.getFirstName(),
                                              playerRegistrationRequest.getLastName(),
                                              new Team(
                                                  "Barcelona", "No logo", "Football team"),
                                              playerRegistrationRequest.getDateOfBirth(),
                                              playerRegistrationRequest.getPhoneNumber(),
                                              playerRegistrationRequest.getWeight(),
                                              playerRegistrationRequest.getHeight(),
                                              playerRegistrationRequest.getGender(),
                                              null
                );

                playerRepo.save(newPlayer);

//            String uploadDir = "user-photos/" + newPlayer.getPlayer_id();
//            try {
//                saveFile(uploadDir,photoPlayer,photoPlayerFile);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

                String token = UUID.randomUUID().toString();

                ConfirmationToken confirmationToken = new ConfirmationToken(
                    token,
                    LocalDateTime.now(),
                    LocalDateTime.now().plusMinutes(15), newUser
                );

                confirmationTokenService.saveConfirmationToken(confirmationToken);

                try {
                    sendVerificationEmail(newUser.getEmail(), newPlayer.getFirstName(), newPlayer.getLastName(), token);
                    sendPlayerPassword(newUser.getEmail(), newPlayer.getFirstName(), newPlayer.getLastName(), password);

                } catch (MessagingException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    private void sendVerificationEmail(String userEmail, String firstName, String lastName, String token)
        throws MessagingException, UnsupportedEncodingException {
        String subject = "Please verify your registration";
        String content = "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;" +
                         "color:#0b0c0c\">\n" +
                         "\n" +
                         "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                         "\n" +
                         "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;" +
                         "min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                         "    <tbody><tr>\n" +
                         "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                         "        \n" +
                         "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;" +
                         "max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                         "          <tbody><tr>\n" +
                         "            <td width=\"70\" bgcolor=\"#1D566E\" valign=\"middle\">\n" +
                         "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" " +
                         "border=\"0\" style=\"border-collapse:collapse\">\n" +
                         "                  <tbody><tr>\n" +
                         "                    <td style=\"padding-left:10px\">\n" +
                         "                  \n" +
                         "                    </td>\n" +
                         "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;" +
                         "padding-left:10px\">\n" +
                         "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;" +
                         "color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your " +
                         "email</span>\n" +
                         "                    </td>\n" +
                         "                  </tr>\n" +
                         "                </tbody></table>\n" +
                         "              </a>\n" +
                         "            </td>\n" +
                         "          </tr>\n" +
                         "        </tbody></table>\n" +
                         "        \n" +
                         "      </td>\n" +
                         "    </tr>\n" +
                         "  </tbody></table>\n" +
                         "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" " +
                         "cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;" +
                         "max-width:580px;width:100%!important\" width=\"100%\">\n" +
                         "    <tbody><tr>\n" +
                         "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                         "      <td>\n" +
                         "        \n" +
                         "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" " +
                         "cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                         "                  <tbody><tr>\n" +
                         "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                         "                  </tr>\n" +
                         "                </tbody></table>\n" +
                         "        \n" +
                         "      </td>\n" +
                         "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                         "    </tr>\n" +
                         "  </tbody></table>\n" +
                         "\n" +
                         "\n" +
                         "\n" +
                         "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" " +
                         "cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;" +
                         "max-width:580px;width:100%!important\" width=\"100%\">\n" +
                         "    <tbody><tr>\n" +
                         "      <td height=\"30\"><br></td>\n" +
                         "    </tr>\n" +
                         "    <tr>\n" +
                         "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                         "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1" +
                         ".315789474;max-width:560px\">\n" +
                         "        \n" +
                         "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi" +
                         " [[name]] ,</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;" +
                         "color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your" +
                         " account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;" +
                         "padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;" +
                         "font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\" [[URL]] \">Activate Now</a> " +
                         "</p></blockquote>\n Link will expire in 15 minutes. <p>See you soon</p>" +
                         "        \n" +
                         "      </td>\n" +
                         "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                         "    </tr>\n" +
                         "    <tr>\n" +
                         "      <td height=\"30\"><br></td>\n" +
                         "    </tr>\n" +
                         "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                         "\n" +
                         "</div></div>"
                         + myCompanyName;

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(companyEmail, myCompanyName);
        helper.setTo(userEmail);
        helper.setSubject(subject);

        content = content.replace("[[name]]", firstName + " " + lastName);
        String verifyURL = siteURL + "/api/v1/accounts/verify?token=" + token;

        content = content.replace("[[URL]]", verifyURL);

        helper.setText(content, true);

        mailSender.send(message);
    }

    private void sendPlayerPassword(String userEmail, String firstName, String lastName, String password)
        throws MessagingException, UnsupportedEncodingException {
        String subject = "Here is yor account password ";
        String content = "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;" +
                         "color:#0b0c0c\">\n" +
                         "\n" +
                         "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                         "\n" +
                         "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;" +
                         "min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                         "    <tbody><tr>\n" +
                         "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                         "        \n" +
                         "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;" +
                         "max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                         "          <tbody><tr>\n" +
                         "            <td width=\"70\" bgcolor=\"#1D566E\" valign=\"middle\">\n" +
                         "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" " +
                         "border=\"0\" style=\"border-collapse:collapse\">\n" +
                         "                  <tbody><tr>\n" +
                         "                    <td style=\"padding-left:10px\">\n" +
                         "                  \n" +
                         "                    </td>\n" +
                         "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;" +
                         "padding-left:10px\">\n" +
                         "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;" +
                         "color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your " +
                         "email</span>\n" +
                         "                    </td>\n" +
                         "                  </tr>\n" +
                         "                </tbody></table>\n" +
                         "              </a>\n" +
                         "            </td>\n" +
                         "          </tr>\n" +
                         "        </tbody></table>\n" +
                         "        \n" +
                         "      </td>\n" +
                         "    </tr>\n" +
                         "  </tbody></table>\n" +
                         "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" " +
                         "cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;" +
                         "max-width:580px;width:100%!important\" width=\"100%\">\n" +
                         "    <tbody><tr>\n" +
                         "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                         "      <td>\n" +
                         "        \n" +
                         "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" " +
                         "cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                         "                  <tbody><tr>\n" +
                         "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                         "                  </tr>\n" +
                         "                </tbody></table>\n" +
                         "        \n" +
                         "      </td>\n" +
                         "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                         "    </tr>\n" +
                         "  </tbody></table>\n" +
                         "\n" +
                         "\n" +
                         "\n" +
                         "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" " +
                         "cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;" +
                         "max-width:580px;width:100%!important\" width=\"100%\">\n" +
                         "    <tbody><tr>\n" +
                         "      <td height=\"30\"><br></td>\n" +
                         "    </tr>\n" +
                         "    <tr>\n" +
                         "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                         "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1" +
                         ".315789474;max-width:560px\">\n" +
                         "        \n" +
                         "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi" +
                         " [[name]] ,</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;" +
                         "color:#0b0c0c\"> Thank you for registering. Please click on the below link to sync your" +
                         " data with our system: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;" +
                         "padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;" +
                         "font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\" [[URL]] \">Sync Now</a> " +
                         "        \n" +
                         "<h3> [[password]] </h3>"+
                         "      </td>\n" +
                         "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                         "    </tr>\n" +
                         "    <tr>\n" +
                         "      <td height=\"30\"><br></td>\n" +
                         "    </tr>\n" +
                         "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                         "\n" +
                         "</div></div>"
                         + myCompanyName;

//        String content = "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;" +
//                         "color:#0b0c0c\">\n" +
//                         "\n" +
//                         "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
//                         "\n" +
//                         "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;" +
//                         "min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
//                         "    <tbody><tr>\n" +
//                         "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
//                         "        \n" +
//                         "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;" +
//                         "max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
//                         "          <tbody><tr>\n" +
//                         "            <td width=\"70\" bgcolor=\"#1D566E\" valign=\"middle\">\n" +
//                         "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" " +
//                         "border=\"0\" style=\"border-collapse:collapse\">\n" +
//                         "                  <tbody><tr>\n" +
//                         "                    <td style=\"padding-left:10px\">\n" +
//                         "                  \n" +
//                         "                    </td>\n" +
//                         "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;" +
//                         "padding-left:10px\">\n" +
//                         "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;" +
//                         "color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Your account password " +
//                         "email</span>\n" +
//                         "                    </td>\n" +
//                         "                  </tr>\n" +
//                         "                </tbody></table>\n" +
//                         "              </a>\n" +
//                         "            </td>\n" +
//                         "          </tr>\n" +
//                         "        </tbody></table>\n" +
//                         "        \n" +
//                         "      </td>\n" +
//                         "    </tr>\n" +
//                         "  </tbody></table>\n" +
//                         "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" " +
//                         "cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;" +
//                         "max-width:580px;width:100%!important\" width=\"100%\">\n" +
//                         "    <tbody><tr>\n" +
//                         "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
//                         "      <td>\n" +
//                         "        \n" +
//                         "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" " +
//                         "cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
//                         "                  <tbody><tr>\n" +
//                         "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
//                         "                  </tr>\n" +
//                         "                </tbody></table>\n" +
//                         "        \n" +
//                         "      </td>\n" +
//                         "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
//                         "    </tr>\n" +
//                         "  </tbody></table>\n" +
//                         "\n" +
//                         "\n" +
//                         "\n" +
//                         "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" " +
//                         "cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;" +
//                         "max-width:580px;width:100%!important\" width=\"100%\">\n" +
//                         "    <tbody><tr>\n" +
//                         "      <td height=\"30\"><br></td>\n" +
//                         "    </tr>\n" +
//                         "    <tr>\n" +
//                         "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
//                         "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1" +
//                         ".315789474;max-width:560px\">\n" +
//                         "        \n" +
//                         "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi" +
//                         " [[name]] ,</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;" +
//                         "<br> Here is your generated password:<br>" +
//                         "<h3>\" [[password]] \"</h3>" +
//                         "color:#0b0c0c\"> Thank you for registering. Please click on the below link to sync your" +
//                         " data with our system: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;" +
//                         "padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;" +
//                         "font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\" [[URL]] \">Activate Now</a>  " +
//                         "        \n" +
//                         "      </td>\n" +
//                         "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
//                         "    </tr>\n" +
//                         "    <tr>\n" +
//                         "      <td height=\"30\"><br></td>\n" +
//                         "    </tr>\n" +
//                         "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
//                         "\n" +
//                         "</div></div>"
//                         + myCompanyName;

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(companyEmail, myCompanyName);
        helper.setTo(userEmail);
        helper.setSubject(subject);

        content = content.replace("[[name]]", firstName + " " + lastName);

        content = content.replace("[[password]]", password);

        content = content.replace("[[URL]]", "https://ec2-157-175-145-254.me-south-1.compute.amazonaws.com:9300/oauth2/authorization/google");


        helper.setText(content, true);

        mailSender.send(message);
    }

    @Transactional
    public String verifiedToken(String token) {
        confirmationTokenService.setConfirmedAt(token);
        Long verifiedUserID = confirmationTokenService.getConfirmationToken(token).get().getUser().getUser_id();
        if (userRepo.findById(verifiedUserID).isPresent()) {
            if (userRepo.findById(verifiedUserID).get().getIsEnabled()) {
                return "Token is already verified ";
            }
            UserCredentials verifiedUser = userRepo.findById(verifiedUserID).get();
            verifiedUser.setIsEnabled(true);
            userRepo.save(verifiedUser);
            return "Token verified successfully.";
        }
        return "Verification not Working";
    }

    public String generatePassword() {
        PasswordGenerator gen = new PasswordGenerator();
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(2);

        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(2);

        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(2);

        CharacterData specialChars = new CharacterData() {
            public String getErrorCode() {
                return ERROR_CODE;
            }

            public String getCharacters() {
                return "!@#$%^&*()_+";
            }
        };
        CharacterRule splCharRule = new CharacterRule(specialChars);
        splCharRule.setNumberOfCharacters(2);

        return gen.generatePassword(10, splCharRule, lowerCaseRule,
                                    upperCaseRule, digitRule);
    }


    public void saveFile(String uploadDir, String fileName,
                                MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IOException("Could not save image file: " + fileName);
        }
    }

    public void photo(MultipartFile file) {
        System.out.println(file);
        String photoPlayer = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        System.out.println("photoPlayer: " + photoPlayer);
        String uploadDir = "user-photos/" + 1;
        try {
            saveFile(uploadDir,photoPlayer,file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

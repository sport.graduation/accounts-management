package com.powerfit.accountsManagement.model;


import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Player implements Serializable {
    @Id
    private Long player_id;

    private String firstName;
    private String lastName;

    @ManyToOne(fetch = FetchType.EAGER,cascade= CascadeType.ALL)
    @JsonBackReference
    @JsonIgnoreProperties(value = {"referenceList", "handler", "hibernateLazyInitializer"}, allowSetters = true)
    @JoinColumn(name = "team_id",nullable = false)
    private Team myTeam;
    private String dateOfBirth;
    private Long phoneNumber;
    private int weight;
    private int height;
    private Boolean gender;
    private String photo;

    private String firebaseToken;


    public Player(Long player_id, String firstName, String lastName, Team myTeam, String dateOfBirth,
                  Long phoneNumber, int weight, int height, Boolean gender, String photo) {
        this.player_id = player_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.myTeam = myTeam;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.weight = weight;
        this.height = height;
        this.gender = gender;
        this.photo = photo;
    }
}

package com.powerfit.accountsManagement.model;

import java.util.Collection;
import java.util.Collections;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


@Getter
@Setter
@NoArgsConstructor
@Entity
public class UserCredentials implements UserDetails {

    @Id
    @SequenceGenerator(
        name = "UserCredentials_sequence",
        sequenceName = "UserCredentials_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        generator = "UserCredentials_sequence",
        strategy = GenerationType.SEQUENCE
    )
    private Long user_id;
    private String password;
    @Column(unique = true)
    private String email;
    private String userRole;
    private Boolean isLocked = false;
    private Boolean isEnabled = false;

    public UserCredentials(String password, String email, String userRole) {
        this.password = password;
        this.email = email;
        this.userRole = userRole;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(userRole);
        return Collections.singleton(simpleGrantedAuthority);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !isLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

}

package com.powerfit.accountsManagement.model;


import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Coach implements Serializable {
    @Id
    private Long coach_id;
    private String photo;
    private int phone_num;

    private String firstName;
    private String lastName;

    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.ALL})
    @JsonIgnoreProperties(value = {"referenceList", "handler", "hibernateLazyInitializer"}, allowSetters = true)
    @JoinColumn(name = "team_id")
    private Team myTeam;

}

package com.powerfit.accountsManagement.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity

@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Team implements Serializable {
    @Id
    @Column(name = "team_id")
    @SequenceGenerator(
        name = "Team_sequence",
        sequenceName = "Team_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        generator = "Team_sequence",
        strategy = GenerationType.SEQUENCE
    )
    private Long team_id;

    private String name;
    private String logo;
    private String description;


    @OneToMany(mappedBy = "myTeam", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Coach> myCoaches;
    @OneToMany(mappedBy = "myTeam", cascade = {CascadeType.ALL})
    @JsonManagedReference
    private Set<Player> myPlayers;


    public Team(String name, String logo, String description) {
        this.name = name;
        this.logo = logo;
        this.description = description;
    }

    Long timeOfCreate = System.currentTimeMillis();

    public Team(Long team_id, String name, String description) {
        this.team_id = team_id;
        this.name = name;
        this.description = description;
    }
}


package com.powerfit.accountsManagement.request;

import com.powerfit.accountsManagement.model.Team;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CoachRegistrationRequest {
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private int phoneNum;
    private Team team;
}

package com.powerfit.accountsManagement.request;

import com.powerfit.accountsManagement.model.Team;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class PlayerRegistrationRequest {

    private String email;
    private String firstName;
    private String lastName;
    private String userRole;

    private Long teamID;
    private String dateOfBirth;
    private Long phoneNumber;
    private Boolean gender;
    private int weight;
    private int height;

}

package com.powerfit.accountsManagement;

import com.powerfit.accountsManagement.registration.token.ConfirmationTokenRepo;
import com.powerfit.accountsManagement.userDetailsRepo.UserRepo;
import com.powerfit.accountsManagement.userDetailsRepo.CoachRepo;
import com.powerfit.accountsManagement.userDetailsRepo.PlayerRepo;
import com.powerfit.accountsManagement.userDetailsRepo.TeamRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableConfigurationProperties
@EnableEurekaClient
@EnableJpaRepositories(basePackageClasses = {UserRepo.class, CoachRepo.class, PlayerRepo.class, TeamRepo.class,
											 ConfirmationTokenRepo.class})
public class AccountsManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountsManagementApplication.class, args);
	}

}

package com.powerfit.accountsManagement.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ConnValidationResponse {
    private boolean isAuthenticated;
    private String username;
}

package com.powerfit.accountsManagement.services;

import java.util.List;

import com.powerfit.accountsManagement.model.Team;
import com.powerfit.accountsManagement.userDetailsRepo.TeamRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TeamService {

    @Autowired
    private TeamRepo teamRepo;

    public Team getTeam(Long id) {
        return teamRepo.getById(id);
    }

    public ResponseEntity<?> updatedTeam(Team updatedTeam , Long id) {

        teamRepo.saveAndFlush(updatedTeam);
        return ResponseEntity.ok("Team updated successfully.");
    }

    public ResponseEntity<?> deleteTeam(Long id) {
        teamRepo.delete(teamRepo.getById(id));
        return ResponseEntity.ok("Team Deleted successfully.");
    }

    public List<Team> getTeams() {
        return teamRepo.findAll();
    }

    public ResponseEntity<?>getTeamInfo(long teamId){
       Team team= teamRepo.getById(teamId);
       Team teamInfo = new Team(team.getTeam_id(),team.getName(),team.getDescription());
       return ResponseEntity.status(HttpStatus.OK).body(teamInfo);
    }

}

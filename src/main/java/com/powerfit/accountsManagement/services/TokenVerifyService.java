package com.powerfit.accountsManagement.services;

import java.util.Optional;

import javax.crypto.SecretKey;

import com.powerfit.accountsManagement.authentication.jwt.JwtConfig;
import com.powerfit.accountsManagement.model.UserCredentials;
import com.powerfit.accountsManagement.security.ConnValidationResponse;
import com.powerfit.accountsManagement.userDetailsRepo.UserRepo;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenVerifyService {

    @Autowired
    UserRepo userRepo;

    private final JwtConfig jwtConfig;
    private final SecretKey secretKey;

    public TokenVerifyService(JwtConfig jwtConfig, SecretKey secretKey) {
        this.jwtConfig = jwtConfig;
        this.secretKey = secretKey;
    }



    public ConnValidationResponse validateToken(String header) {
        String token = header.replace(jwtConfig.getTokenPrefix(), "");

        String email = Jwts.parser()
                           .setSigningKey(secretKey)
                           .parseClaimsJws(token)
                           .getBody()
                           .getSubject();

        Optional<UserCredentials> userOptional = userRepo.findByEmail(email);
        if (userOptional.isPresent()) {
            if (userOptional.get().isCredentialsNonExpired()) {
                return ConnValidationResponse.builder().username(userOptional.get().getUsername()).isAuthenticated(true)
                                             .build();
            } else {
                ConnValidationResponse.builder().username(userOptional.get().getUsername()).isAuthenticated(false)
                                      .build();
            }
        }

        return ConnValidationResponse.builder().username(": User not found!").isAuthenticated(false)
                                     .build();
    }
}

package com.powerfit.accountsManagement.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerfit.accountsManagement.request.updatePasswordRequest;
import com.powerfit.accountsManagement.userDetailsRepo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UpdatePasswordService {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    public void playerEditPassword(String newPassword, Long id) {
        ObjectMapper objectMapper = new ObjectMapper();
        updatePasswordRequest updatePasswordRequest = new updatePasswordRequest();
        try {
            updatePasswordRequest = objectMapper.readValue(newPassword, updatePasswordRequest.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        boolean userExist = userRepo.findById(id).isPresent();
        if (!userExist) {
            throw new IllegalStateException("User with the given ID: " + id + " isn't exist");
        }
        boolean userEnabled = userRepo.findById(id).get().getIsEnabled();
        if (!userEnabled)
            throw new IllegalStateException("User with the given ID: " + id + " is inactive");

        userRepo.updatePassword(id, passwordEncoder.encode(updatePasswordRequest.getPassword()));
    }
}

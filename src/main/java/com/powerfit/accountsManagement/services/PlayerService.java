package com.powerfit.accountsManagement.services;

import java.util.List;
import java.util.Optional;

import com.powerfit.accountsManagement.model.Player;
import com.powerfit.accountsManagement.model.Team;
import com.powerfit.accountsManagement.userDetailsRepo.PlayerRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PlayerService {

    @Autowired
    private PlayerRepo playerRepo;

    public ResponseEntity<?> getPlayerTeam(long playerId) {
        if(playerRepo.findById(playerId).isPresent()) {
            Optional<Long> playerTeamId = Optional.ofNullable(playerRepo.findById(playerId).get().getMyTeam().getTeam_id());
            return ResponseEntity.status(HttpStatus.OK).body(playerTeamId);
        }else
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    public ResponseEntity<?> updatedPlayer(Player updatedPlayer) {
        playerRepo.saveAndFlush(updatedPlayer);
        return ResponseEntity.ok("Team updated successfully.");
    }

    public ResponseEntity<?> deletePlayer(Long id) {
        playerRepo.delete(playerRepo.getById(id));
        return ResponseEntity.ok("Team Deleted successfully.");
    }

    public ResponseEntity<?> getPlayers() {
        log.info("Trying to gate Players data.");
        return ResponseEntity.status(HttpStatus.OK).body(playerRepo.findAll());

    }

    public ResponseEntity<?> getPlayer(Long id) {
        log.info("Trying to gate Player data with ID: {}", id);
        return ResponseEntity.status(HttpStatus.OK).body(playerRepo.findById(id));
    }

    public ResponseEntity<?> saveMyFirebaseToken(String token, long id) {

        boolean userExist = playerRepo.findById(id).isPresent();
        if (!userExist) {
            throw new IllegalStateException("User with the given ID: " + id + " isn't exist");
        }

        playerRepo.updateFirebaseToken(id, token);
        return ResponseEntity.ok("Token saved successfully.");
    }
}

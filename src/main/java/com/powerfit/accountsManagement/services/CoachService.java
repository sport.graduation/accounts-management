package com.powerfit.accountsManagement.services;

import java.util.List;

import com.powerfit.accountsManagement.model.Coach;
import com.powerfit.accountsManagement.userDetailsRepo.CoachRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CoachService {

    @Autowired
    private CoachRepo coachRepo;

    public ResponseEntity<?> updatedCoach(Coach updatedcoach) {
        coachRepo.saveAndFlush(updatedcoach);
        return ResponseEntity.ok("Team updated successfully.");
    }

    public ResponseEntity<?> deleteCoach(Long id) {
        coachRepo.delete(coachRepo.getById(id));
        return ResponseEntity.ok("Team Deleted successfully.");
    }

    public List<Coach> getCoachs() {
        coachRepo.findAll();
        return null;
    }

    public Coach getCoach(Long id) {
        coachRepo.findById(id);
        return null;
    }
}

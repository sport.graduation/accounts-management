package com.powerfit.accountsManagement.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import javax.crypto.SecretKey;

import com.powerfit.accountsManagement.authentication.jwt.JwtConfig;
import com.powerfit.accountsManagement.model.UserCredentials;
import com.powerfit.accountsManagement.userDetailsRepo.UserRepo;
import io.jsonwebtoken.Jwts;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserService {

    @Autowired
    UserRepo userRepo;

    private final JwtConfig jwtConfig;
    private final SecretKey secretKey;

    public UserService(JwtConfig jwtConfig, SecretKey secretKey) {
        this.jwtConfig = jwtConfig;
        this.secretKey = secretKey;
    }

    public UserCredentials getUserByToken(String header) {

        String token = header.replace(jwtConfig.getTokenPrefix(), "");

        String email = Jwts.parser()
                           .setSigningKey(secretKey)
                           .parseClaimsJws(token)
                           .getBody()
                           .getSubject();

        Optional<UserCredentials> userCredentials = userRepo.findByEmail(email);

        return userCredentials
            .orElseThrow(() -> new UsernameNotFoundException("User Not found: " + email));
    }

    public MultipartFile getPhotosImagePath(Long id) throws IOException {
        MultipartFile multipartFile = null;
        if (userRepo.findById(id).isPresent()) {
           // String photoName = userRepo.findById(id).get().getPersonalPhotoName();
            String photoPath = "user-photos/1/ahmad al-hamwi.jpg";
            File file = new File(photoPath);
            FileInputStream input = new FileInputStream(file);
            multipartFile = new MockMultipartFile("fileItem",
                                                  file.getName(), "image/png", input.readAllBytes());
        }
        return multipartFile;
    }


}
